<?php
require_once("vendor/autoload.php");
// Autoload files using Composer autoload
include_once("City.php");

class CityTest extends \PHPUnit\Framework\TestCase

{
   public function testgetCityNameById()
   {
      $city = new City();
      $result = $city->getCityNameById(1);
      $expected = 'Bordeaux';
      $this->assertTrue($result == $expected);
   } 
}

 